﻿using System;

namespace SimlpeCalculator
{
    class Program
    {
        static double Add(double a, double b)
        {
            return a + b;
        }

        static double Substract(double a, double b)
        {
            return a - b;
        }

        static double Multiply(double a, double b)
        {
            return a * b;
        }

        static double Divide(double a, double b)
        {
            return a / b;
        }

        static void Main(string[] args)
        {
            int x; 
            double a, b, result = 0;

            do
            {
                Console.WriteLine("Enter the first operand: ");
                a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the second operand: ");
                b = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the operation you want to perform:\n 1 - adding\n 2 - substracting\n 3 - multiplying\n 4 - dividing\n 0 - finish");
                x = Convert.ToInt32(Console.ReadLine());
  
                switch (x)
                {
                    case 1:
                        result = Add(a, b);
                        Console.WriteLine(a + " + " + b + " = " + result);
                        break;
                    case 2:
                        result = Substract(a, b);
                        Console.WriteLine(a + " - " + b + " = " + result);
                        break;
                    case 3:
                        result = Multiply(a, b);
                        Console.WriteLine(a + " * " + b + " = " + result);
                        break;
                    case 4:
                        result = Divide(a, b);
                        Console.WriteLine(a + " / " + b + " = " + result);
                        break;
                    default:
                        Console.WriteLine("There is no such operation. Enter another value.");
                        break;
                }
            } while (x != 0);
            
        }
    }
}
